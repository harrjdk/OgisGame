package net.jesdevtest.ogisgame.items;

import android.graphics.drawable.Drawable;
import android.location.Location;

import net.jesdevtest.ogis.ogisgame.R;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;

public class MapResourceItem extends Marker implements UserTappableItem {
    private Marker player;
    private String mSnippet;
    public MapResourceItem(String aTitle, String aSnippet, GeoPoint aGeoPoint, Marker player, MapView map, Drawable icon) {
        super(map);
        this.player = player;
        this.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        this.setIcon(icon);
        this.setPosition(aGeoPoint);
        this.setTitle(aTitle);
        this.mSnippet = aSnippet;
    }

    @Override
    public void userTapped(int index) {

    }

    @Override
    public void userLongPressed(int index) {

    }

    @Override
    public String toString() {
        GeoPoint thisPoint = getPosition();
        GeoPoint playerPoint = player.getPosition();
        float[] results = new float[1];
        Location.distanceBetween(thisPoint.getLatitude(),thisPoint.getLongitude(),playerPoint.getLatitude(),playerPoint.getLongitude(),results);
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%S\n", getTitle()));
        sb.append(String.format("%s\n", mSnippet));
        sb.append(String.format("%.2f m away", results[0]));
        return sb.toString();
    }
}
