package net.jesdevtest.ogisgame.items;

public interface UserTappableItem {

    void userTapped(final int index);

    void userLongPressed(final int index);
}
